#include <iostream>
#include <chrono>


const int N = 10000;

class Timer // подсчет времени
{
private:

    using clock_t = std::chrono::high_resolution_clock;
    using second_t = std::chrono::duration<double, std::ratio<1> >;

    std::chrono::time_point<clock_t> m_beg;

public:
    Timer() : m_beg(clock_t::now())
    {
    }

    void reset()
    {
        m_beg = clock_t::now();
    }

    double elapsed() const
    {
        return std::chrono::duration_cast<second_t>(clock_t::now() - m_beg).count();
    }
};




template<typename U>
class List{
public:
    List();
    ~List();
    int GetSize(){return Size;}
    void push_back(U data);
    void pop_front();
    U& operator[](const int index);
    void clear();

private:


    template<typename T>
    class Node{
    public:
        Node *pNext;
        T data;

        Node(T data = T(), Node *pNext = nullptr){
            this->data= data;
            this->pNext = pNext;
        }

    };

    int Size;
    Node<U> *head;



};

template<typename T>
List<T>::List() {
    Size = 0;
    head = nullptr;

}
template<typename T>
List<T>::~List() {
    clear();
}

template<typename T>
void List<T>::push_back(T data) {
    Size ++;
    if( head == nullptr){
        head = new Node<T>(data);
    }
    else{
        Node<T> *current = this->head;
        while(current->pNext != nullptr){
            current = current->pNext ;
        }
        current->pNext = new Node<T>(data);


    }

}

template<typename U>
U &List<U>::operator[](const int index) {
    int counter = 0;
    Node<U> *current = this->head;
    while(current != nullptr){
       if(counter == index){
           return current -> data;
       }
       current = current -> pNext;
       counter++;
    }

}

template<typename T>
void List<T>::pop_front() {
    Node <T> *temp = head;
    head = head->pNext;
    delete temp;

    Size--;

}

template<typename T>
void List<T>::clear() {
    while(Size != 0)
        pop_front();

}


int main() {


    int k;
    std::cout<<"Введите число, которое нужно найти в этом списке: ";
    std::cin>> k;



    List<int> lst;
    for(int i = 0; i < N; i ++)
        lst.push_back(rand()%100);

    /*int mas[N];
    for(int i = 0; i < N ; i++)
        mas[i] = rand()%100;*/

    int kolVoNumb = 0;

    Timer time;

    for(int i = 0; i < N; i++)
        if(lst[i] == k)
            kolVoNumb ++;
        /*if(mas[i]==k)
            kolVoNumb++;*/


    std::cout << "Time elapsed: " << time.elapsed() << '\n';



    std::cout<<"Нужных чисел в списке: "<<kolVoNumb<<std::endl;



    return 0;
}

//при поиске количества чисел в списках:
//Время поиска в списке: ~0.0521848
//Время поиска в массиве: ~5.5516e-05 или 0.0374064